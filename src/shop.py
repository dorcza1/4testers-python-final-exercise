class Order:
    def __init__(self, customer_email):
        self.customer_email = customer_email
        self.products = []
        self.purchased = False

    def add_product(self, product):
        self.products.append({
            "name": product.name,
            "unit_price": product.unit_price,
            "quantity": product.quantity
        })

    def get_total_price(self):
        total_price = 0
        for product in self.products:
            total_price = total_price + (product["unit_price"] * product["quantity"])
        return total_price


    def get_total_quantity_of_products(self):
        total_quantity = 0
        for product in self.products:
            total_quantity = total_quantity + product["quantity"]
        return total_quantity

    def purchase(self):
        self.purchased = True


class Product:
    def __init__(self, name, unit_price, quantity=1):
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    def get_price(self):
        return self.unit_price * self.quantity

if __name__ == '__main__':
    shoes = Product("Shoes", 30.00, 3)
    tshirt = Product('T-Shirt', 50.00, 2)
    bag = Product('Bag', 10.00)
    socks = Product("Socks", 10.50)
    dress = Product("Dress", 150.00, 2)

    print(shoes.get_price())
    print(tshirt.get_price())
    print(bag.get_price())


    order_1 = Order("abc@example.com")

    print(order_1.customer_email)
    print(order_1.products)
    print(order_1.purchased)

    order_1.add_product(shoes)
    order_1.add_product(tshirt)
    order_1.add_product(bag)
    order_1.add_product(socks)
    order_1.add_product(dress)
    order_1.purchase()

    print(order_1.customer_email)
    print(order_1.products)
    print(order_1.purchased)

    print(Order.get_total_price(order_1))
    print(Order.get_total_quantity_of_products(order_1))